import re
from spellchecker import SpellChecker
import language_tool_python
import json


class Checker:
    def __init__(self):
        self.spell = SpellChecker(language=None)
        self.spell.word_frequency.load_dictionary('./wordlists/dutch.json')
        self.spell.word_frequency.remove_by_threshold(10)
        self.languagetool = language_tool_python.LanguageTool('nl-BE')

        self.classnumbers = [
            "1.01",
            "1.02",
            "1.03",
            "1.04",
            "1.05",
            "1.06",
            "1.07",
            "1.08",
            "1.09",
            "1.10",
            "1A",
            "2A1",
            "2A2",
            "2A3",
            "2B1",
            "2B2",
            "2B3",
            "2C1",
            "2C2",
            "2C3",
            "TILE",
        ]

        self.whitelist_pyspellchecker_words = {"saelens", "cybersecurity", "chamilo"}

        self.whitelist_languagetool_categories = {"COLLOQUIALISMS", "TYPOS", "OUDERWETS"}

        self.whitelist_languagetool_rule_ids = {"GEDACHTESTREEPJE"}

    def check_correct_greeting(self, email, errors):
        if not re.search("^(Geachte |Beste )", email, re.MULTILINE):
            if re.search("^(Geachte|Beste),", email, re.MULTILINE):
                errors.append("Naam ontvanger vergeten bij aanhef (\"Geachte <ontvanger>,\" / \"Beste <ontvanger>,\").")
            else:
                errors.append("Geen geldige aanhef (\"Geachte <ontvanger>,\" / \"Beste <ontvanger>,\").")

    def check_correct_signature(self, email, errors):
        if not re.search("^(Met vriendelijke groet(en)?|Hoogachtend),", email, re.MULTILINE):
            errors.append(
                "Geen geldige groet (\"Hoogachtend,\" / \"Met vriendelijke groeten,\" / \"Met vriendelijke groet,\")."
            )

            if re.search("mvg", email, re.MULTILINE | re.IGNORECASE):
                errors.append("Mvg is geen geldige groet.")

    def check_name_present(self, email, errors, debug):
        matches = re.findall("([A-z][a-z]+) ([A-z][a-z]+)", email, re.MULTILINE)
        if not matches:
            errors.append("Er is geen naam vermeld.")
        else:
            name = matches[-1]
            debug.append(f"Found name: {name}")
            return name

    def check_classnumber_present(self, email, errors, debug):
        found_classnumbers = []
        for classnumber in self.classnumbers:
            found_classnumbers.append(email.rfind(classnumber))

        max_i = -1
        max_rindex = -1
        for i, rindex in enumerate(found_classnumbers):
            if rindex > max_rindex:
                max_i = i
                max_rindex = rindex

        if max_i != -1:
            debug.append(f"Classnumber detected: {self.classnumbers[max_i]}")
        else:
            errors.append("Er is geen klasnummer vermeld.")

    def check_correct_spelling(self, email, errors, debug, name):

        ### pyspellchecker

        firstname = name[0].lower()
        lastname = name[1].lower()

        words = self.spell.split_words(email)
        debug.append(f"Words detected by pyspellchecker: {words}")

        for word in self.spell.unknown(words):
            if word not in {firstname, lastname} and word not in self.whitelist_pyspellchecker_words:
                debug.append(f"\"{word}\" is spelled incorrectly.")
                correction = self.spell.correction(word)
                candidates = self.spell.candidates(word)
                errors.append(f"\"{word}\" is niet correct gespeld. Bedoelt u \"{correction}\"? (pyspellchecker)")

        ### languagetool.org

        matches = self.languagetool.check(email)

        for match in matches:
            if match.category not in whitelist_languagetool_categories and match.ruleId not in whitelist_languagetool_rule_ids:
                debug.append(f"Found match: {self.languagetool_math_tostring(match)}")
                replacements = ", ".join(["\"" + r + "\"" for r in match.replacements])
                flagged_text = email[match.offset:(match.offset + match.errorLength)]
                message = match.message.rstrip(".")

                error = f"Taalfout voor \"{flagged_text}\" op index {match.offset}: {message}. Bedoelt u een van deze suggesties: {replacements}? (languagetool.org)"
                errors.append(error)
            else:
                debug.append(f"Skipped match: {self.languagetool_math_tostring(match)}")

    def languagetool_math_tostring(self, match):
        match_json = {
            'ruleId': match.ruleId,
            'message': match.message,
            'replacements': match.replacements,
            'offsetInContext': match.offsetInContext,
            'context': match.context,
            'offset': match.offset,
            'errorLength': match.errorLength,
            'category': match.category,
            'ruleIssueType': match.ruleIssueType,
            'sentence': match.sentence
        }

        return json.dumps(match_json, indent=4)

    def check(self, email):
        email = email.strip()

        errors = []
        debug = []

        self.check_correct_greeting(email, errors)
        self.check_correct_signature(email, errors)
        name = self.check_name_present(email, errors, debug)
        self.check_classnumber_present(email, errors, debug)
        self.check_correct_spelling(email, errors, debug, name)

        return (errors, debug)
