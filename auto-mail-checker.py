#!/usr/bin/env python3

from flask import Flask
from flask import render_template
from flask import request
from .checker import Checker

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def root():
    output = None
    debug = None
    email = "Plak hier de email."

    if request.method == "POST":
        email = request.form["email"]
        checker = Checker()
        (errors, debug) = checker.check(email)

        output = "\n".join(errors)
        debug = "\n".join(debug)

    return render_template("index.html", email=email, output=output, debug=debug)
