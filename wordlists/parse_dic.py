#!/usr/bin/env python3

import json

INPUT_FILENAME = "nl_full.txt"
OUTPUT_FILENAME = "dutch.json"
REFERENCE_WORDLIST_FILENAME = "opentaal_wordlist.txt"

reference = set()
with open(REFERENCE_WORDLIST_FILENAME) as reference_wordlist:
    for word in reference_wordlist.read().splitlines():
        reference.add(word.lower())

with open(INPUT_FILENAME) as input:
    content = {}

    lines = input.read().splitlines()
    added = 0
    skipped = 0
    for i, line in enumerate(lines):
        (word, frequency) = line.split()
        frequency = int(frequency)

        if word.lower() in reference:
            content[word] = frequency
            added = added + 1
        else:
            # print(f"(line: {i}/{len(lines)}) Warning: \"{word}\" was found in input, but not in reference. Skipped.")
            skipped = skipped + 1

    with open(OUTPUT_FILENAME, "w") as output:
        json.dump(content, output, sort_keys=True, indent=4)

    print(f"Words added: {added}")
    print(f"Words Skipped: {skipped}")
